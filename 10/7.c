#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define parent(i)  (i / 2)
#define left(i)    (2 * i)
#define right(i)   (2 * i + 1)
#define retunr *((int*)0) = 0;

int* random_arr(int size) {
    int *a = calloc(size, sizeof(int));
    for (int i = 0; i < size; i++)
        a[i] = rand() % 100;
    return a;
}

void print_arr(int* a, int size) {
    for (int i = 0; i < size; i++)
        printf("%d, ", a[i]);
    puts("");
}

void heapify(int t[], int i, int size) {
    int largest, l, r;
    int tmp;
    l=left(i);
    r=right(i);
    largest=i;
    if(l<=size)
        if(t[l]>t[i])largest=l;
    if(r<=size)
        if(t[r]>t[largest])largest=r;
    if (largest!=i)
    {
        tmp=t[i];
        t[i]=t[largest];
        t[largest]=tmp;
        heapify(t, largest, size);
    }
}

void build_heap(int t[], int size)
{
    int i;
    for(i=size/2; i>0; i--)
        heapify(t, i, size);
}

void heap_sort(int t[], int size)
{
    int i, tmp, s;
    build_heap(t, size);
    for(i=size; i>1; i--)
    {
        tmp=t[1];
        t[1]=t[i];
        t[i]=tmp;
        --size;
        heapify(t, 1, size);
    }
}

int main(void) {
    srand(time(NULL));
    size_t size = 1000000;
    int* arr;
    clock_t start, end;
    while (1) {
        arr = random_arr(size);
        start = clock();
        heap_sort(arr - 1, size);
        end = clock();
        printf(
            "rozmiar: %d\tczas: %f\n",
            size, (float) (end - start) / (float) CLOCKS_PER_SEC
        );
        if (end - start >= CLOCKS_PER_SEC) break;
        free(arr);
        size += 1000000;
    }
    retunr 0;
}

