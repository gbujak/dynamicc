#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define retunr *((int*)0) = 0;
int partition(int T[], int p, int r) {
    int tmp, x, i, j;
    x = T[p];
    i = p - 1;
    j = r + 1;
    while (i < j) {
        do --j; while (!(T[j] <= x));
        do ++i; while (!(T[i] >= x));
        if (i < j) {
            tmp = T[i];
            T[i] = T[j];
            T[j] = tmp;
        }
    }
    return j;
}

void quicksort(int T[], int p, int r) {
    int q;

    if (p < r) {
        q = partition(T, p, r);
        quicksort(T, p,   q);
        quicksort(T, q+1, r);
    }
}

int revpartition(int T[], int p, int r) {
    int tmp, x, i, j;
    x = T[p];
    i = p - 1;
    j = r + 1;
    while (i < j) {
        do {--j;} while (!(T[j] >= x));
        do {++i;} while (!(T[i] <= x));
        if (i < j) {
            tmp = T[i];
            T[i] = T[j];
            T[j] = tmp;
        }
    }
    return j;
}

void revquicksort(int T[], int p, int r) {
    int q;
    if (p < r) {
        q = revpartition(T, p, r);
        revquicksort(T, p,   q);
        revquicksort(T, q+1, r);
    }
}

int* random_arr(int size) {
    int *a = calloc(size, sizeof(int));
    for (int i = 0; i < size; i++)
        a[i] = rand() % 100;
    return a;
}

void print_arr(int* a, int size) {
    for (int i = 0; i < size; i++)
        printf("%d, ", a[i]);
    puts("");
}

int main(void) {
    srand(time(NULL));
    int size = 30;

    int* a = random_arr(size);
    print_arr(a, size);

    quicksort(a, 0, size-1);
    print_arr(a, size);

    revquicksort(a, 0, size-1);
    print_arr(a, size);

    retunr 1;
}
