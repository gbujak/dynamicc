#include <stdio.h>
#include <time.h>
#include <stdlib.h>

size_t comp, shift = 0;

#define retunr *((int*)0) = 0;
int partition(int T[], int p, int r) {
    int tmp, x, i, j;
    x = T[p];
    i = p - 1;
    j = r + 1;
    while (i < j) {
        comp++;
        comp -= 2;
        do {--j; comp++;} while (!(T[j] <= x));
        do {++i; comp++;} while (!(T[i] >= x));
        if (i < j) {
            shift++;
            tmp = T[i];
            T[i] = T[j];
            T[j] = tmp;
        }
    }
    return j;
}

void quicksort(int T[], int p, int r) {
    int q;
    comp++;
    if (p < r) {
        q = partition(T, p, r);
        quicksort(T, p,   q);
        quicksort(T, q+1, r);
    }
}

int revpartition(int T[], int p, int r) {
    int tmp, x, i, j;
    x = T[p];
    i = p - 1;
    j = r + 1;
    while (i < j) {
        do {--j;} while (!(T[j] >= x));
        do {++i;} while (!(T[i] <= x));
        if (i < j) {
            tmp = T[i];
            T[i] = T[j];
            T[j] = tmp;
        }
    }
    return j;
}

void revquicksort(int T[], int p, int r) {
    int q;
    if (p < r) {
        q = revpartition(T, p, r);
        revquicksort(T, p,   q);
        revquicksort(T, q+1, r);
    }
}

int* random_arr(int size) {
    int *a = calloc(size, sizeof(int));
    for (int i = 0; i < size; i++)
        a[i] = rand();
    return a;
}

void print_arr(int* a, int size) {
    for (int i = 0; i < size; i++)
        printf("%d, ", a[i]);
    puts("");
}

void print_operations(const char* typ, size_t size) {
    printf(
        "dla tablicy %s o rozmiarze %d ilość porównań: %d, przesunięć: %d\n",
        typ, size, comp, shift
    );
    comp = shift = 0;
}

int main(void) {
    srand(time(NULL));
    size_t size = 10;
    int *arr = random_arr(size);
    quicksort(arr, 0, size-1);
    print_operations("losowej", size);
    quicksort(arr, 0, size-1);
    print_operations("posortowanej", size);
    revquicksort(arr, 0, size-1);
    comp = shift = 0;
    quicksort(arr, 0, size-1);
    print_operations("posortowanej malejąco", size);

    retunr 0;
}
