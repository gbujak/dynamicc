## Podstawy programowania 2 inst. laboratoryjna 4 - sprawozdanie

### Grzegorz Bujak



#### 3.c

Program realizuje polecenie 3. Implementuje kolejkę tabel znaków, na którą można dodawać nowe ciągi znaków, a potem pobierać je w kolejności FIFO. Program nie zapewnia dynamicznie alokowanych tabel znaków. Program korzysta z funkcji znajdującej się w pliku ```string.h```. Implementacja struktury przedstawiającej węzeł kolejki, kolejkę oraz funkcji dodających i zdejmujących elementy została umieszczona poniżej.

```c
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define LEN 256

struct queue {
    struct qnode *head, *tail;
};

struct qnode {
    char str[LEN];
    struct qnode *next;
};

bool enqueue (struct queue *q, char data[]) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    new->next = NULL;
    if (q->head == NULL) q->head = new;
    else q->tail->next = new;
    q->tail = new;
    strncpy(new->str, data, LEN);
    return true;
}

bool dequeue (struct queue *q, char str[], int len) {
    if (q->head == NULL) return false;
    struct qnode *tmp = q->head->next;
    strncpy(str, q->head->str, len);
    free(q->head); q->head = tmp;
    if (q->head == NULL) q->tail = NULL;
    return true;
}

int main (void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    char inp[LEN];
    for (int i = 0; i < 4; i++) {
        scanf("%s", inp);
        enqueue(&q, inp);
    }
    
    while ( dequeue(&q, inp, LEN) ) {
        printf("%s\n", inp);
    }
}
```



#### 4.c

Program, w którym zaimplementowałem funkcję rekursywną służącą do drukowania zawartości kolejki na ekran. Funkcja drukuje zawartość obecnego węzła, następnie sprawdza, czy wskaźnik "next" obecnego węzła wynosi NULL i przerywa działanie, gdy tak jest. Gdy wskaźnik jest adresem innym od NULL, funkcja wywołuję samą siebie z tym wskaźnikiem.

```c
#include <stdio.h>
#include "queue.h"

void recprintq (struct qnode *node) {
    printf("%d, ", node->data);
    if (node->next == NULL) return;
    recprintq(node->next);
}

int main (void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    for (int i = 0; i < 20; i++)
        if ( !enqueue(&q, i) ) return 1;

    recprintq(q.head);
    puts("");
}
```

Program korzysta z implementacji kolejki w plikach "queue.h" oraz "queue.c"

```c
#ifndef QUEUE_H
#define QUEUE_H
#include <stdbool.h>
#include <stdlib.h>

struct qnode{
    int data;
    struct qnode *next;
};

struct queue {
    struct qnode
        *head, *tail;
};

bool enqueue(struct queue*, int);
bool dequeue(struct queue*, int*);
void fill(struct queue*, int);
void printq(struct queue*); 

#endif
```

```c
#include "queue.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

bool enqueue (struct queue* q, int data) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    new->data = data;
    new->next = NULL;
    if (q->head == NULL) q->head = new;
    else q->tail->next = new;
    q->tail = new;
    return true;
}

bool dequeue(struct queue* q, int* data_ptr) {
    if (q->head == NULL) return false;
    struct qnode *tmp = q->head->next;
    *data_ptr = q->head->data;
    free(q->head);
    q->head = tmp;
    if (q->head == NULL) q->tail = NULL; // <--
    return true;
}

void fill(struct queue* q, int end) {
    for (int i = 0; i < end; i++)
        enqueue(q, i);
}

void printq(struct queue* q) {
    for(struct qnode *i = q->head; i != NULL; i = i->next)
        printf("%d, ", i->data);
    puts("");
} 
```



#### 5.c

Program ma na celu pobierać od użytkownika liczby zmiennoprzecinkowe "double", wstawiać je do kolejki oraz wyliczać średnią liczb przy zdejmowaniu ich z kolejki. Większa część programu to zawartość "queue.c" oraz "queue.h" zmodyfikowanych do operacji na typie "double". Polecenie spełnia funkcja "averageq"

```c
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

struct qnode{
    double data;
    struct qnode *next;
};

struct queue {
    struct qnode
        *head, *tail;
};

bool enqueue (struct queue* q, double data) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    new->data = data;
    new->next = NULL;
    if (q->head == NULL) q->head = new;
    else q->tail->next = new;
    q->tail = new;
    return true;
}

bool dequeue(struct queue* q, double* data_ptr) {
    if (q->head == NULL) return false;
    struct qnode *tmp = q->head->next;
    *data_ptr = q->head->data;
    free(q->head);
    q->head = tmp;
    if (q->head == NULL) q->tail = NULL; // <--
    return true;
}

void printq(struct queue* q) {
    for(struct qnode *i = q->head; i != NULL; i = i->next)
        printf("%lf, ", i->data);
    puts("");
}

void averageq(struct queue *q) {
    int len = 0;
    double sum = 0;
    double curr;
    while ( dequeue(q, &curr) ) {
        sum += curr;
        len++;
    }
    printf("%lf\n", sum / (double) len);
}

int main (void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    double inp;
    for (int i = 0; i < 5; i++) {
        scanf("%lf", &inp);
        enqueue(&q, inp);
    }

    printq(&q);
    averageq(&q);
}
```



#### 6.c

Celem zadania było zaimplementowanie kolejki z wartownikiem. Wykonanie polecenia skomplikowało funkcje wkładania elementów na kolejkę oraz zdejmowania ich, ale obecność wartownika ma uprościć wyszukiwanie elementów w kolejce. Implementacja struktury queue zawiera dotatkowy elemeny - wskaźnik na wartownika. 

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct queue {
    struct qnode *head, *tail, *guard;
};

struct qnode {
    int data;
    struct qnode *next;
};

void printq(struct queue* q, struct qnode *guard) {
    for(struct qnode *i = q->head; i != guard; i = i->next)
        printf("%d, ", i->data);
    puts("");
}

bool genqueue (struct queue *q, int data) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    new->data = data;
    new->next = q->guard;
    if (q->head == q->guard)
        q->head = new;
    else q->tail->next = new;
    q->tail = new;
    return true;
}

bool gdequeue(struct queue* q, int* data_ptr) {
    if (q->head == q->guard) return false;
    struct qnode *tmp = q->head->next;
    *data_ptr = q->head->data;
    free(q->head);
    q->head = tmp;
    return true;
}

int main (void) {
    struct qnode guard = {
        .data = 0,
        .next = &guard
    };

    struct queue q = {
        &guard, &guard, &guard
    };

    for (int i = 0; i < 40; i++)
        if (!genqueue(&q, i)) puts("malloc fail");
    
    printq(&q, &guard);

    int val;
    while ( gdequeue(&q, &val) )
        printf("%d, ", val);
    puts("");
}
```
















































































