#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct queue {
    struct qnode *head, *tail, *guard;
};

struct qnode {
    int data;
    struct qnode *next;
};

void printq(struct queue* q, struct qnode *guard) {
    for(struct qnode *i = q->head; i != guard; i = i->next)
        printf("%d, ", i->data);
    puts("");
}

bool genqueue (struct queue *q, int data) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    new->data = data;
    new->next = q->guard;
    if (q->head == q->guard)
        q->head = new;
    else q->tail->next = new;
    q->tail = new;
    return true;
}

bool gdequeue(struct queue* q, int* data_ptr) {
    if (q->head == q->guard) return false;
    struct qnode *tmp = q->head->next;
    *data_ptr = q->head->data;
    free(q->head);
    q->head = tmp;
    return true;
}

int main (void) {
    struct qnode guard = {
        .data = 0,
        .next = &guard
    };

    struct queue q = {
        &guard, &guard, &guard
    };

    for (int i = 0; i < 40; i++)
        if (!genqueue(&q, i)) puts("malloc fail");
    
    printq(&q, &guard);

    int val;
    while ( gdequeue(&q, &val) )
        printf("%d, ", val);
    puts("");
}
