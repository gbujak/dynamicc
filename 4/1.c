#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"

bool pop_tail(struct queue* q, int* data_ptr) {
    if (q->tail == NULL) return false;
    struct qnode *pretail = q->head;
    while (pretail->next != q->tail) pretail = pretail->next;
    pretail->next = NULL;
    *data_ptr = q->tail->data;
    free(q->tail);
    q->tail = pretail;
    return true;
}

int main(void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    fill(&q, 10);
    printq(&q);
    int data;
    pop_tail(&q, &data);
    printq(&q);
    printf("popped %d\n", data);
}
