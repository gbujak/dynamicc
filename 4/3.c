#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define LEN 256

struct queue {
    struct qnode *head, *tail;
};

struct qnode {
    char str[LEN];
    struct qnode *next;
};

bool enqueue (struct queue *q, char str[]) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    strncpy(new->str, str, LEN);
    if (q->head == NULL) q->head = new;
    else q->tail->next = new;
    q->tail = new;
    return true;
}

bool dequeue (struct queue *q, char str[], int len) {
    if (q->head == NULL) return false;
    struct qnode *tmp = q->head->next;
    strncpy(str, q->head->str, len);
    free(q->head); q->head = tmp;
    if (q->head == NULL) q->tail = NULL;
    return true;
}

int main (void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    char inp[LEN];
    for (int i = 0; i < 4; i++) {
        scanf("%s", inp);
        enqueue(&q, inp);
    }

    char temp[LEN];
    while (dequeue(&q, temp, LEN))
        printf("%s, ", temp);
    puts("");
}