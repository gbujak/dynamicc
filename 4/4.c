#include <stdio.h>
#include "queue.h"

void recprintq (struct qnode *node) {
    printf("%d, ", node->data);
    if (node->next == NULL) return;
    recprintq(node->next);
}

int main (void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    for (int i = 0; i < 20; i++)
        if ( !enqueue(&q, i) ) return 1; // return in main == abort()
           // ABORT IF ANY FAIL

    recprintq(q.head);
    puts("");
}
/*
    >>> ./a.out 
    >>> 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
*/