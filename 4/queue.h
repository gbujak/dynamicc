#ifndef QUEUE_H
#define QUEUE_H
#include <stdbool.h>
#include <stdlib.h>

struct qnode{
    int data;
    struct qnode *next;
};

struct queue {
    struct qnode
        *head, *tail;
};

bool enqueue(struct queue*, int);
bool dequeue(struct queue*, int*);
void fill(struct queue*, int);
void printq(struct queue*); 

#endif