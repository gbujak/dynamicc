#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"

bool push_head(struct queue* q, int data) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    if (q->head == NULL) q->tail = new;
    new->next = q->head;
    new->data = data;
    q->head = new;
    return true;
}

int main(void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    fill(&q, 10);
    printq(&q);
    int data;
    scanf("%d", &data);
    push_head(&q, data);
    printq(&q);
    printf("pushed %d\n", data);
}