// To samo, co w nadfolderze ale double zamiast int

#ifndef QUEUE_H
#define QUEUE_H
#include <stdbool.h>
#include <stdlib.h>

struct qnode{
    double data;
    struct qnode *next;
};

struct queue {
    struct qnode
        *head, *tail;
};

bool enqueue(struct queue*, double);
bool dequeue(struct queue*, double*);
void printq(struct queue*); 

#endif