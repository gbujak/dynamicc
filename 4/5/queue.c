// To samo, co w nadfolderze ale double zamiast int

#include "queue.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

bool enqueue (struct queue* q, double data) {
    struct qnode *new = malloc(sizeof *new);
    if (new == NULL) return false;
    new->data = data;
    new->next = NULL;
    if (q->head == NULL) q->head = new;
    else q->tail->next = new;
    q->tail = new;
    return true;
}

bool dequeue(struct queue* q, double* data_ptr) {
    if (q->head == NULL) return false;
    struct qnode *tmp = q->head->next;
    *data_ptr = q->head->data;
    free(q->head);
    q->head = tmp;
    if (q->head == NULL) q->tail = NULL; // <--
    return true;
}

void printq(struct queue* q) {
    for(struct qnode *i = q->head; i != NULL; i = i->next)
        printf("%lf, ", i->data);
    puts("");
} 


