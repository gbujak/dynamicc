#include "queue.h"
#include <stdio.h>

#define LEN 5

void printdel (struct queue *q) {
    double sum = 0;
    struct qnode *i = q->head;
    while (i != NULL) {
        struct qnode *temp = i->next;
        sum += i->data;
        free(i);
        i = temp;
    }
    printf("average -> %lf\n", sum / (double) LEN);
}

int main (void) {
    struct queue q = {
        .head = NULL,
        .tail = NULL
    };

    double temp;
    for (int i = 0; i < LEN; i++) {
        scanf("%lf", &temp);
        if (!enqueue(&q, temp))
            return 1;
    }

    printq(&q);

    printdel(&q);
}