#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void print_arr(int* arr, int size) {
    for (int i = 0; i < size; i++)
        printf("%d, ", arr[i]);
    puts("");
}

void swap(int* a, int* b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

int partition (int* arr, int lo, int hi) {
    int pivot = arr[hi];
    int p_index = lo;
    for (int i = lo; i < hi; i++)
        if (arr[i] < pivot)
            swap(&arr[p_index++], &arr[i]);
    swap(&arr[p_index], &arr[hi]);
    return p_index;
}

void quicksort(int* arr, int lo, int hi) {
    if (lo < hi) {
        int p = partition(arr, lo, hi);
        quicksort(arr, lo, p - 1);
        quicksort(arr, p + 1, hi);
    }
}

int main (void) {
    srand(time(NULL));

    int size = 20;
    int* arr = calloc(size, sizeof(int));
    for (int i = 0; i < size; i++) arr[i] = rand() % 21;

    puts("test");
    print_arr(arr, size);
    quicksort(arr, 0, size - 1);
    print_arr(arr, size);
}

