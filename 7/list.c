#include <stdio.h>
#include <stddef.h> //Definicja NULL
#include <stdlib.h>
#define LTYPE int

typedef struct node {
    LTYPE val;
    struct node* next;
    struct node* prev;
} node;

typedef struct list {
    node* start;
    node* end;
} list;

/*
 * Operacje podstawowe na listach:
 * - utworzenie nowej listy           -> create
 * - dodanie nowego elementu do listy -> add
 * - usunięcie elementu z listy       -> delete
 * - usunięcie listy                  -> remove
 */

list create() {
    return (list) {
        NULL, NULL
    };
}

node* add_rec(node* node, LTYPE x) {
    if (node == NULL) {
        node = malloc(sizeof(node));
        node->next = node->prev = NULL;
        node->val = x;
        return node;
    }
    if (node->next != NULL) return add_rec(node->next, x);
    else {
        node->next = malloc(sizeof(node));
        node->next->prev = node;
        node = node->next;
        node->val = x;
        node->next = NULL;
        return node;
    }
}

void add(list* ls, LTYPE x) {
    ls->end = add_rec(ls->end, x);
    if (ls->start == NULL)
        ls->start = ls->end;
}

/*
 * compare_func to wskaźnik na funkcję, która
 * zwraca 0, gdy jej argumenty są równe
 */
void delete_rec(
        node* node, LTYPE x,
        int (*compare_func)(LTYPE, LTYPE)
        ) {
    if (node == NULL) return;
    if (!compare_func(node->val, x)) {
        if (node->prev != NULL)
            node->prev->next = node->next;
        if (node->next != NULL)
            node->next->prev = node->prev;
    }
    delete_rec(node->next, x, compare_func);
    if (!compare_func(node->val, x)) free(node);
}

void listremove(node* node) {
    if (node == NULL) return;
    listremove(node->next);
    free(node);
}

void printls(node* node) {
    if (node == NULL) {
        printf("\n");
        return;
    }
    printf("%d, ", node->val);
    printls(node ->next);
}

int compare(int a, int b) {
    return !(a == b);
}

int main(void){
    list ls = create();
    for (int i = 0; i < 25; i++) {
        add(&ls, i);
        add(&ls, i);
    }

    printls(ls.start);

    delete_rec(ls.start, 10, &compare);
    delete_rec(ls.start, 9, &compare);

    printls(ls.start);

    listremove(ls.start);
}
