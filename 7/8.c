#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
    char val;
    struct node* next;
    struct node* prev;
} node;

node* add(node* node, char val) {
    while (node != NULL && node->next != NULL)
        node = node->next;
    if (node == NULL) {
        node = malloc(sizeof(node));
        node ->prev = NULL;
    } else {
        node->next = malloc(sizeof(node));
        node->next->prev = node;
        node = node->next;
    }
    node->next = NULL;
    node->val = val;
    return node;
}

node* get_last(node* node) {
    while (node->next != NULL)
        node = node->next;
    return node;
}

bool palindrome(node* left, node* right) {
    if (left->val != right->val) return false;
    else if (left == right || left->next == right) return true;
    else return palindrome(left->next, right->prev);
}

void printls(node* node) {
    if (node != NULL) fputc(node->val, stdout);
    else fputc('\n', stdout);
    if (node != NULL) printls(node->next);
}

int main(void) {
    char input = fgetc(stdin);
    node* ls = NULL;
    if (input != '\n') ls = add(ls, input);
    while ((input = fgetc(stdin)) != '\n') {
        add(ls, input);
    }
    printls(ls);
    printf(
        "palindrom? %s",
        (palindrome(ls, get_last(ls)))
        ? "prawda\n"
        : "fałsz\n"
    );
}