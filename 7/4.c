#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node* prev;
    struct node* next;
} node;

typedef struct list {
    node *start, *end;
} list;

void push_back (list* ls, int val) {
    node* new = malloc(sizeof(node));
    new->prev = ls->end;
    new->next = NULL;
    if (ls->end != NULL)
        ls->end->next = new;
    else ls->start = new;
    ls->end = new;
    new->val = val;
}

void push_front (list* ls, int val) {
    node* new = malloc(sizeof(node));
    new->next = ls->start;
    new->prev = NULL;
    if (ls->start != NULL)
        ls->start->prev = new;
    else ls->end = new;
    ls->start = new;
    new->val = val;
}

bool empty (list *ls) {
    return (ls->start == NULL && ls->end == NULL);
}

int pop_back (list* ls) {
    if (ls->end == NULL) return 0;
    node* temp = ls->end;
    ls->end = ls->end->prev;
    if (ls->end == NULL) ls->start = NULL;
    int val = temp->val;
    free(temp);
    return val;   
}

int pop_front (list* ls) {
    if (ls->start == NULL) return 0;
    node* temp = ls->start;
    ls->start = ls->start->next;
    if (ls->start == NULL) ls->end = NULL;
    int val = temp->val;
    free(temp);
    return val;   
}

int main(void) {
    list ls;
    for (int i = 0; i < 5; i++)
        push_back(&ls, i);
    puts("test");
    for (int i = 0; i < 5; i++)
        push_front(&ls, i);
    for (int i = 0; i < 3; i++)
        pop_back(&ls);
    while (!empty(&ls))
        printf("%d, ", pop_front(&ls));
    puts("");    
}