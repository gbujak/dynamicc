#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
    char val[51];
    struct node *prev, *next;
} node;

node* push(node *n, char* inp) {
    node *new = malloc(sizeof(node));
    if (new == NULL) puts("malloc error");
    new->prev = n;
    new->next = NULL;
    if (n != NULL)
        n->next = new;
    new->val[0] = '\0';
    strcpy(new->val, inp);
    return new;
}

void printls(node *n, int rev) {
    while (n != NULL) {
        printf("%s, ", n->val);
        if (rev) n = n->prev;
        else n = n->next;
    }
    puts("");
}

int main(void) {
    char input[51];
    node* start;
    node* end;
    
    scanf("%s", input);
    if(strcmp(input, "koniec") != 0)
        start = push(NULL, input);
    scanf("%s", input);
    end = start;
    while (strcmp(input, "koniec") != 0) {
        end = push(end, input);
        scanf("%s", input);
    }
    printls(start, 0);
    printls(end, 1);
}
