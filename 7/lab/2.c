#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node *prev, *next;
} node;

node* push (node *top, int val) {
    node* new = malloc(sizeof(node));
    new->prev = top;
    new->next = NULL;
    if (top != NULL) top->next = new;
    new->val = val;
    return new;
}

int pop(node** top) {
    if (*top == NULL) return -1;
    node* temp = *top;
    if (temp->prev != NULL)
        temp->prev->next = NULL;
    *top = temp->prev;
    int val = temp->val;
    free(temp);
    return val;
}

int main(void) {
    node *stack = NULL;
    for (int i = 0; i < 10; i++)
        stack = push(stack, i);
    for (int i = 0; i < 10; i++)
        printf("%d, ", pop(&stack));
    puts("");
}
