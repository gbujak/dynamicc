#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node *prev, *next;
} node;

typedef struct list {
    node *front, *end;
} list;

void add (list* ls, int val) {
    node* new = malloc(sizeof(node));
    if (!new) puts("malloc error");
    if (ls->end != NULL) ls->end->next = new;
    else ls->front = new;
    new->prev = ls->end;
    new->next = NULL;
    new->val = val;
    ls->end = new;
}

void printls(list* ls) {
    node* node = ls->front;
    while (node != NULL) {
        printf("%d, ", node->val);
        node = node->next;
    }
    puts("");
}

void delete(list* ls, int val) {
    for (node* i = ls->front; i != NULL;){
        if (i->val == val) {
            if (i->prev != NULL) i->prev->next = i->next;
            if (i->next != NULL) i->next->prev = i->prev;
            if (i == ls->front) ls->front = ls->front->next;
            if (i == ls->end) ls->end = ls->end->prev;
            node* temp = i;
            i = i->next;
            free (temp);
        }
    }
}

int main(void) {
    list ls;
    for (int i = 0; i < 10; i++)
        add(&ls, i);
    add(&ls, 3);
    add(&ls, 5);
    delete(&ls, 5);
    printls(&ls);
}






