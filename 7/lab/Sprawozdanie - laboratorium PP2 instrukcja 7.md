## Sprawozdanie - laboratorium PP2 instrukcja 7

##### Grzegorz Bujak 1ID11B



#### Zadanie 1.

Operacje na liście dwukierunkowej zaimplementowane w sposób rekurencyjny.

```c
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node *prev, *next;
} node;

typedef struct list {
    node *front, *end;
} list;

void add (list* ls, int val) {
    node* new = malloc(sizeof(node));
    if (!new) puts("malloc error");
    if (ls->end != NULL) ls->end->next = new;
    else ls->front = new;
    new->prev = ls->end;
    new->next = NULL;
    new->val = val;
    ls->end = new;
}

void printls(list* ls) {
    node* node = ls->front;
    while (node != NULL) {
        printf("%d, ", node->val);
        node = node->next;
    }
    puts("");
}

void delete(list* ls, int val) {
    for (node* i = ls->front; i != NULL;){
        if (i->val == val) {
            if (i->prev != NULL) i->prev->next = i->next;
            if (i->next != NULL) i->next->prev = i->prev;
            if (i == ls->front) ls->front = ls->front->next;
            if (i == ls->end) ls->end = ls->end->prev;
            node* temp = i;
            i = i->next;
            free (temp);
        }
    }
}

int main(void) {
    list ls;
    for (int i = 0; i < 10; i++)
        add(&ls, i);
    add(&ls, 3);
    add(&ls, 5);
    delete(&ls, 5);
    printls(&ls);
}
```



#### Zadanie 2.

Stos na bazie listy dwukierunkowej.

```c
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node *prev, *next;
} node;

node* push (node *top, int val) {
    node* new = malloc(sizeof(node));
    new->prev = top;
    new->next = NULL;
    if (top != NULL) top->next = new;
    new->val = val;
    return new;
}

int pop(node** top) {
    if (*top == NULL) return -1;
    node* temp = *top;
    if (temp->prev != NULL)
        temp->prev->next = NULL;
    *top = temp->prev;
    int val = temp->val;
    free(temp);
    return val;
}

int main(void) {
    node *stack = NULL;
    for (int i = 0; i < 10; i++)
        stack = push(stack, i);
    for (int i = 0; i < 10; i++)
        printf("%d, ", pop(&stack));
    puts("");
}
```



#### Zadanie 6.

Program w dwukierunkowej liście liniowej zapisuje podane od użytkownika wyrazy i drukuje je w kolejności podania oraz odwrotnej.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
    char val[51];
    struct node *prev, *next;
} node;

node* push(node *n, char* inp) {
    node *new = malloc(sizeof(node));
    if (new == NULL) puts("malloc error");
    new->prev = n;
    new->next = NULL;
    if (n != NULL)
        n->next = new;
    new->val[0] = '\0';
    strcpy(new->val, inp);
    return new;
}

void printls(node *n, int rev) {
    while (n != NULL) {
        printf("%s, ", n->val);
        if (rev) n = n->prev;
        else n = n->next;
    }
    puts("");
}

int main(void) {
    char input[51];
    node* start;
    node* end;
    
    scanf("%s", input);
    if(strcmp(input, "koniec") != 0)
        start = push(NULL, input);
    scanf("%s", input);
    end = start;
    while (strcmp(input, "koniec") != 0) {
        end = push(end, input);
        scanf("%s", input);
    }
    printls(start, 0);
    printls(end, 1);
}
```

   
