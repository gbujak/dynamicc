#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int push(struct Stack *stack, char x) {
    if (stack->top >= stack->max) {
        puts("stack overflow");
        return -1;
    } else {
        stack->arr[stack->max++] = x;
        return 0;
    }
}

char pop(struct Stack *stack) {
    return stack->arr[stack->max--];
}

struct Node *dpush(struct Node *top, char x) { // top can equal NULL
    // ↓↓↓ https://stackoverflow.com/questions/17258647/why-is-it-safer-to-use-sizeofpointer-in-malloc
    struct Node *temp = malloc(sizeof *temp);
    temp->previous = top;
    temp->content = x;
    return temp;
}

char dpop(struct Node **top_ptr) {
    struct Node *top = *top_ptr;
    if(top == NULL) {
        puts("Popping off an empty stack");
        abort();
    }

    *top_ptr = top->previous;
    char x = top->content;
    free(top);
    return x;
}
