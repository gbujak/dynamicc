#ifndef STACK
#define STACK

#define SIZE 100

struct Stack {
    int max;
    int top;
    char arr[SIZE];
};

int push(struct Stack*, char);

char pop(struct Stack*);

struct Node {
    char content;
    struct Node *previous;
};

// d -> dynamic ↓

struct Node *dpush(struct Node *top, char x);

char dpop(struct Node **top_ptr);

#endif