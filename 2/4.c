#include <stdio.h>
#include <string.h>
#include "stack.h"

#define LEN 4000

int is_eq(char table[], struct Node *stack) {
    int result = 1;
    for (int i = 0; i < strlen(table) && stack != NULL; i++) {
        if (table[i] != dpop(&stack)) {
            result = 0;
        }
    }
    return result; 
}

struct Node *put_on_stack(char str[], struct Node *stack) {
    for (int i = 0; i < strlen(str); i++) {
        stack = dpush(stack, str[i]);
    }
    return stack;
}

int main(void) {
    char input[LEN];
    fgets(input, LEN, stdin);
    input[strlen(input) - 1] = '\0'; // removes '\n' from end
    struct Node *stack = put_on_stack(input, NULL);

    printf("palindrom? %d\n", is_eq(input, stack));
}