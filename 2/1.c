#include <stdio.h>
#include "stack.h"

int main (void) {
    char input;
    struct Node *stack_top = NULL;
    while ((input = fgetc(stdin)) != '\n') {
        stack_top = dpush(stack_top, input);
    }

    while(stack_top != NULL) {
        fputc( dpop(&stack_top), stdout );
    }
    puts(""); //newline
}