#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct node {
    int key;
    struct node
        *left, *right, *parent;
} node;

node* rec_add(node* root, int key) {
    if (root == NULL) {
        root = malloc(sizeof (node));
        root->key = key;
        root->left 
            = root->right 
                = root->parent 
                    = NULL;
        return root;
    } else if (root->key < key) {
        if (root->right == NULL) {
            node* temp = malloc(sizeof (node));
            root->right = temp;
            temp->parent = root;
            temp->left 
                = temp->right
                    = NULL;
            temp->key = key;
        } else {
            rec_add(root->right, key);
        }
    } else if (root->key > key) {
        if (root->left == NULL) {
            node* temp = malloc(sizeof (node));
            root->left = temp;
            temp->parent = root;
            temp->left 
                = temp->right
                    = NULL;
            temp->key = key;
        } else {
            rec_add(root->left, key);
        }
    }
    return root;
}

node* top(node* n) {
    if (n == NULL) {
        puts("finding top of NULL list!!!");
        abort();
    }
    while (n->parent != NULL) n = n->parent;
    return n;
}

node* add(node* tree, int val) {
    if (tree == NULL) {
        tree = malloc(sizeof (node));
        tree->key = val;
        tree->left
            = tree->right
                = tree->parent
                    = NULL;
        return tree;
    }
    node** temp;
    while (1) {
        if (tree->key == val) return top(tree);
        temp = (val > tree->key)
                ? &tree->right
                : &tree->left;
        if (*temp == NULL) break;
        tree = *temp;
    }
    *temp = malloc(sizeof (node));
    (*temp)->parent = tree;
    tree = *temp;
    tree->key = val;
    tree->left = tree->right = NULL;
    return top(tree);
}

int max(int a, int b) {
    return (a > b) ? a : b;
}

int get_depth(node* root) {
    int left = -1;
    int right = -1;
    if (root == NULL) return 0;
    if (root->left == NULL && root->right == NULL)
        return 1;
    if (root->right != NULL)
        right = get_depth(root->right);
    if (root->left != NULL)
        left = get_depth(root->left);
    return 1 + max(left, right);
}

void print_tree(node* root, int depth) {
    if (root == NULL) return;

    print_tree(root->right, depth + 1);

    for (int i = 0; i < depth; i++)
        printf("\t");
    printf("%2d\n", root->key);

    print_tree(root->left, depth + 1);
}

node* r_right(node* rotating) {
    if (rotating == NULL) {
        puts("rotating NULL");
        abort();
    }
    node* child = rotating->left;
    node* parent = rotating->parent;
    if (child == NULL) return top(rotating);
    rotating->left = child->right;
    if (rotating->left != NULL)
        rotating->left->parent = rotating;
    child->right = rotating;
    child->parent = parent;
    rotating->parent = child;
    if (parent != NULL)
        if (parent->right == rotating)
            parent->right = child;
        else parent->left = child;
    return top(child);    
}

node* r_left(node* rotating) {
    if (rotating == NULL) {
        puts("rotating NULL");
        abort();
    }
    node* child = rotating->right;
    node* parent = rotating->parent;
    if (child == NULL) return top(rotating);
    rotating->right = child->left;
    if (rotating->right != NULL)
        rotating->right->parent = rotating;
    child->left = rotating;
    child->parent = parent;
    rotating->parent = child;
    if (parent != NULL)
        if (parent->right == rotating)
            parent->right = child;
        else parent->left = child;
    return top(child);    
}

void pre_order(node* n) {
    if (n == NULL) return;
    printf("%d, ", n->key);
    pre_order(n->left);
    pre_order(n->right);
    if (n->parent == NULL)
        printf("\n");
}

void in_order(node* n) {
    if (n == NULL) return;
    in_order(n->left);
    printf("%d, ", n->key);
    in_order(n->right);
    if (n->parent == NULL)
        printf("\n");
}

void post_order(node* n) {
    if (n == NULL) return;
    post_order(n->left);
    post_order(n->right);
    printf("%d, ", n->key);
    if (n->parent == NULL)
        printf("\n");
}

node* tree_to_list(node* tree) {
    node* n = tree;
    int node_count = 0;
    int rotation_count = 0;
    int i = 0;

    while (n != NULL) {
        if (n->left != NULL) {
            r_right(n);
            n = n->parent;
        } else {
            node_count++;
            n = n->right;
        }
    }

    return top(tree);
}

node* dsw(node* tree) {
    node* n = tree;
    unsigned int node_count = 0;
    int rotation_count;
    int i = 0;

    while (n != NULL) {
        if (n->left != NULL) {
            r_right(n);
            n = n->parent;
        } else {
            node_count++;
            n = n->right;
        }
    }

    rotation_count = node_count + 1 - pow(2, log2(node_count+1));
    tree = top(tree);
    n = tree;

    for (i = 0; i < node_count; i++) {
        r_left(n);
        n = n->parent->right;
    }

    node_count -= rotation_count;

    while (node_count > 1) {
        node_count >>= 1;
        n = top(tree);
        for (i = 0; i < node_count; i++){
            r_left(n);
            n = n->parent->right;
        }
    }

    return top(tree);
}

int main(void) {
    int input[] = {4, 2, 6, 1, 3, 5, 7, 8, 9};

    node* tree = NULL;

    //srand(time(NULL));
    //for (int i = 0; i < 10; i++) tree = rec_add(tree, rand() % 100);
    for (int i = 0; i < sizeof(input) / sizeof(input[0]); i++)
        tree = add(tree, input[i]);

    printf("depth: %d\n", get_depth(tree));

    print_tree(tree, 0);
    printf("pre-order:  ");
    pre_order(tree);
    printf("in-order:   ");
    in_order(tree);
    printf("post-order: ");
    post_order(tree);

    puts("------------------ right rotation -------------------");

    tree = r_right(tree->right);
    print_tree(tree, 0);

    puts("------------------ left rotation -------------------");

    tree = r_left(tree->right->right);
    print_tree(tree, 0);

    puts("-------------------- prostowanie --------------------");
    tree = tree_to_list(tree);
    print_tree(tree, 0);

    puts("------------------ dsw ---------------------------");
    tree = dsw(tree);
    print_tree(tree, 0);
}