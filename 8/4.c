#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LEN 50

typedef struct person {
    char name[LEN];
    char surname[LEN];
    unsigned age;
    struct person *next, *prev;
} person;

person* put_behind(person* per, person behind) {
    person* new = malloc(sizeof(person));
    memcpy(new, &behind, sizeof(person));

    new->prev = per->prev;
    new->next = per;

    new->prev->next = new;
    per->prev = new;
    
    return new;
}

int compare_person(person* left, person* right) {
    if (strcmp(left->surname, right->surname) < 0
     || strcmp(left->name, right->name) < 0
     || left->age < right->age) return -1;
    else return 1;
}

person* get_first(person* p_start) {
    person* p = p_start;
    person* min = p_start;
    do {
        if (compare_person(min, p) < 0)
            min = p;
    } while(p != p_start);
    return min;
}

person* add(person* p_start, person val) {
    if (p_start == NULL) {
        person* new = malloc(sizeof(person));
        memcpy(new, &val, sizeof(person));
        new->next = new->prev = new;
        return new;
    }
    person* p = p_start;
    do {
        if (compare_person(p, &val) > 0) break;
    } while (p != p_start);
    return get_first(put_behind(p, val));
}

void printlist(person* p_start) {
    person* p = p_start;
    do {
        printf("%s, %s, %d\n", p->name, p->surname, p->age);
        p = p->next;
    } while (p != p_start);
}

int main(void) {
    person* p = NULL;
    p = add(p, (person){"jan", "kowalski", 50});
    p = add(p, (person){"jan", "kowalski", 30});
    p = add(p, (person){"jam", "kowalski", 60});
    p = add(p, (person){"jam", "kowalski", 50});
    p = add(p, (person){"zzz", "aaa", 100});
    printlist(p);
}