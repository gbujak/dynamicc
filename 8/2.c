#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node* next;
} node;

node* add(node* n, int val) {
    node* new = malloc(sizeof(node));
    new->val = val;
    if (n == NULL) {
        new->next = new;
    } else {
        new->next = n->next;
        n->next = new;
    }
    return new;
}

void printlist(node* n) {
    if (n == NULL) return;
    node* start = n;
    printf("%d, ", n->val);
    n = n->next;
    while (n != start) {
        printf("%d, ", n->val);
        n = n->next;
    }
    puts("");
}

void purge(node* n, node* start) {
    if (n == NULL || n == start) return;
    purge(n->next, start);
    free(n);
}

int main(void) {
    node* n = NULL;
    for (int i = 0; i < 100; i++)
        n = add(n, i);
    printlist(n->next);
    purge(n->next, n);
}