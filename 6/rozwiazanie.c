#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node node;
typedef struct node {
    int data;
    node* next;
} node;

void delete(node* n) {
    if (n == NULL) return;
    delete(n->next);
    free(n);
}

node* append(node* n, int data) {
    if (n == NULL) n = malloc(sizeof(node));
    else {
        n->next = malloc(sizeof(node));
        n = n->next;
    }
    n->data = data;
    n->next = NULL;
    return n;
}

node* fill() {
    node* start;
    node* n;
    start = n = append(NULL, 0);
    n = append(n, 0);
    for (int i = 1; i < 20; i++) {
        n = append(n, i);
        n = append(n, i);
    }
    return start;
}

void rec_print(node* n) {
    if (n != NULL) {
        printf("%d, ", n->data);
        rec_print(n->next);
    } else printf("\n");
}

node* get_end(node *n) {
    if (n->next == NULL) return n;
    else return get_end(n->next);
}

// Rozwiąznie polecenia 1
bool del_last_of(node* n, int val, node *prev) {
    if (n == NULL) return false;
    if (
        n->data == val
        && del_last_of(n->next, val, n) == false
    ) {
        prev->next = n->next;
        free(n);
        return true;
    } else {
        return del_last_of(n->next, val, n);
    }
}

// Rozwiąznie polecenia 2
int sum(node *n) {
    if (n == NULL) return 0;
    else return n->data + sum(n->next);
}

// Rozwiąznie polecenia 3
int node_count(node *n) {
    if (n == NULL) return 0;
    else return 1 + node_count(n->next);
}

// Rozwiąznie polecenia 4
int max(node *n) {
    if (n->next == NULL) return n->data;
    else {
        int max_of_rest = max(n->next);
        return (max_of_rest > n->data) ?
            max_of_rest : n->data;
    } 
}

// Rozwiąznie polecenia 5
int min(node *n) {
    if (n->next == NULL) return n->data;
    else {
        int max_of_rest = max(n->next);
        // Różnica tylko w porównaniu "<"
        return (max_of_rest < n->data) ?
            max_of_rest : n->data;
    } 
}

// Rozwiązanie polecenia 6
void _double(node* n) {
    if (n == NULL) return;
    n->data *= 2;
    _double(n->next);
}

// Rozwiązanie polecenia 7
void print_even(node *n) {
    if (n == NULL) return;
    if ((n->data % 2) == 0) printf("%d, ", n->data);
    print_even(n->next);
}
void print_uneven(node *n) {
    if (n == NULL) return;
    if ((n->data % 2) == 1) printf("%d, ", n->data);
    print_uneven(n->next);
}

// Rozwiązanie polecenia 8
int num_even(node* n) {
    if (n == NULL) return 0;
    if (n->data % 2 == 0) return 1 + num_even(n->next);
    else return num_even(n->next);
}
int num_uneven(node* n) {
    if (n == NULL) return 0;
    if (n->data % 2 == 1) return 1 + num_uneven(n->next);
    else return num_uneven(n->next);
}

// Rozwiązanie polecenia 9
int sum_even(node* n) {
    if (n == NULL) return 0;
    if (n->data % 2 == 0) return n->data + sum_even(n->next);
    else return sum_even(n->next);
}
int sum_uneven(node* n) {
    if (n == NULL) return 0;
    if (n->data % 2 == 1) return n->data + sum_uneven(n->next);
    else return sum_uneven(n->next);
}

// Rozwiązanie polecenia 11
node* copy_list(node* dest, node* src,
                int (*operation)(int)) {
    if (src == NULL) return NULL;
    int val = src->data;
    if (operation != NULL) val = operation(val);
    node* n = append(dest, val);
    copy_list(n, src->next, operation);
    return n;
}
int times2(int x) {return x*2;}


int main(void) {
    node* n = fill();
    puts("lista początkowa");
    rec_print(n);
    del_last_of(n, 10, NULL);
    puts("po usunięciu ostatniej liczby 10");
    rec_print(n);
    printf("suma wynosi: %d\n", sum(n));
    printf("liczba węzłów: %d\n", node_count(n));
    printf("największy element listy: %d\n", max(n));
    printf("najmniejszy element listy: %d\n", min(n));
    _double(n);
    printf("po podwojeniu: \n");
    rec_print(n);

    puts("====================================");

    delete(n);
    n = fill();
    rec_print(n);

    puts("liczby parzyste w liście:");
    print_even(n);
    puts("\nliczby nieparzyste w liście:");
    print_uneven(n);
    puts("");

    printf(
        "liczba parzystych: %d, nieparzystych: %d\n",
        num_even(n),
        num_uneven(n)
    );
    del_last_of(n, 10, NULL);
    printf(
        "liczba parzystych: %d, nieparzystych: %d\n",
        num_even(n),
        num_uneven(n)
    );

    printf(
        "suma parzystych:  %d, nieparzystych: %d\n",
        sum_even(n),
        sum_uneven(n)
    );

    node* temp = fill();
    copy_list(
        get_end(n),
        temp, &times2
    );
    delete(temp);
    puts("===================================");
    puts("lista z dodaną listą");
    rec_print(n);
}