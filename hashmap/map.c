#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
    int val;
    char* key;
    struct node* next;
};

typedef struct {
    int len;
    struct node** tab;
} map;

int hash(const char* key, int top) {
    int hash = 0;
    for (int i = 0; i < strlen(key); i++) 
        hash += (int) key[i];
    return hash % top;
}

void add(map* m, const char* key, int val) {
    if (m->len == -1) {
        puts("adding to removed map");
        abort();
    }
    int index = hash(key, m->len);
    struct node* n = m->tab[index];
    if (n == NULL) {
        m->tab[index] = malloc(sizeof(struct node));
        m->tab[index]->val = val;
        m->tab[index]->next = NULL;
        m->tab[index]->key = malloc((strlen(key) + 1) * sizeof(char));
        strcpy(m->tab[index]->key, key);
    } else {
        while (1) {
            if (strcmp(n->key, key) == 0) {
                n->val = val;
                return;
            } else if (n->next == NULL) break;
            n = n->next;
        }
        n->next = malloc(sizeof(struct node));
        n = n->next;
        n->val = val;
        n->key = malloc((strlen(key) + 1) * sizeof(char));
        strcpy(n->key, key);
        n->next = NULL;
    }
}

int get(map* m, const char* key) {
    int index = hash(key, m->len);
    struct node* n = m->tab[index];
    while (n != NULL) {
        if (strcmp(n->key, key) == 0)
            return n->val;
        n = n->next;
    }
    puts("did not find key");
}

map create(int len) {
    map m;
    m.len = len;
    m.tab = calloc(m.len, sizeof(struct node));
    return m;
}

void freelist(struct node* n) {
    if (n == NULL) return;
    freelist(n->next);
    free(n->key);
    free(n);
}

void purge(map* m) {
    for (int i = 0; i < m->len; i++){
        freelist(m->tab[i]);
    }
    free(m->tab);
    m->len = -1;
}

int main (void) {
    map m = create(10);
    
    add(&m, "test", 69);  
    printf("%d\n", get(&m, "test"));

    add(&m, "bbbb", 10);
    add(&m, "bacb", 20);
    // hash(bbbb) == hash(bacb)

    printf("%d\n", get(&m, "bbbb"));
    printf("%d\n", get(&m, "bacb"));

    purge(&m);
}