#include <stdio.h>
#include <stdlib.h>

double divide(double a, double b) {
	if (b == 0) {
		puts("dzielenie przez 0");
		abort();
	}
	return a/b;
}

double mult(double a, double b) {
	return a*b;
}

int main(void){
	double (*div_ptr)(double, double) = &divide;	
	double (*mult_ptr)(double, double) = &mult;	
	
	int a, b;

	scanf("%d", &a);
	scanf("%d", &b);

	printf(
		"dzielenie -> %f\nmnozenie -> %f\n",
		div_ptr(a, b),
		mult_ptr(a, b)
	);
}
