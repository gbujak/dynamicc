###### Grzegorz Bujak, grupa 1ID11B

# PP_2 laboratorium 1 sprawozdanie



### Cel laboratorium

Podczas laboratorium pisaliśmy programy, które korzystały z dynamicznego przydzielania pamięci, jakie oferuje język C. Celem laboratorium było sprawdzenie w praktyce wiedzy teoretycznej uzyskanej na wykładach oraz utrwalenie wiedzy.

Funkcje, których używaliśmy znajdują się w pliku nagłówkowym "stdlib.h" i są to:

- malloc - funkcja przydziela pamięć na stercie i zwraca do niej wskaźnik

- calloc - to samo co malloc tylko pamięć jest zerowana i funkcja wymaga kolejnego argumentu

- realloc - funkcja przyjmuje wskaźnik na dynamicznie przydzieloną pamięć oraz rozmiar w bajtach. Funkcja przydziela pamięć o podanym rozmiarze, przepisuje do niej zawartość pamięci pod podanym wskaźnikiem, zwalnia starszą pamięć i zwraca wskaźnik do nowszej.

Poniżej zostały opisane wszystkie programy, które napisałem na laboratorium.



---



## 1.c

Celem zadania było stworzenie struktury reprezentującej podstawowe informacje o człowieku:

```c
typedef struct Struktura {
	char imie[20];
	char nazwisko[20];
	int wiek;
} Struktura;
```

Dynamiczne przypisanie pamięci dla struktur w języku C jest dość proste. Nie jest wymagane przydzielanie pamięci dla każdej zmiennej struktury, wystarczające jest użycie funkcji malloc oraz sizeof w następujący sposób:

```c
Struktura *struktura;
struktura = malloc (sizeof(Struktura));
```

ten fragment kodu zajmie na stercie pamięć potrzebną do przechowywania wszystkich zmiennych struktury oraz przypisze wskaźnik na tą pamięć do zmiennej "struktura". W programie napisałem jeszcze dwie funkcje, które zczytują od użytkownika dane, którymi zostaje zapełniona struktura oraz drukują strukturę w terminalu.



---



## 2.c

W tym zadaniu mieliśmy użyć wskaźników na funkcje. Napisałem dwie funkcje o zgodnych typach - jedna zwracająca wynik dzielenia swoich argumentów, a druga - mnożenia. 

```c
double divide(double a, double b) {
	if (b == 0) {
		puts("dzielenie przez 0");
		abort();
	}
	return a/b;
}

double mult(double a, double b) {
	return a*b;
}
```

Użyłem dwóch różnych wskaźników, lecz zgodność typów funkcji pozwala na przypisanie wskaźnika do jednej z nich do zmiennej, wywołanie jej przez wskaźnik, a następnie przypisanie wskaźnika do drugiej funkcji do tej samej zmiennej. 

```c
double (*div_ptr)(double, double) = & divide;
double (*mult_ptr)(double, double) = &mult;
```

wynik funkcji wypisałem na ekran.



---



## pdf-15.c

Najbardziej skomplikowany program, jaki napisałem na laboratorium. Miał na celu utrwalenie wiadomości o funkcji realloc. Funkcja ta jest używana zazwyczaj przy dynamicznie alokowanych tablicach i tak użyliśmy jej w tym programie.

Naszym zadaniem było napisać program, który będzie pobierał ze standardowego wejścia nieokreśloną liczbę znaków i dopisywał je do tabeli tworząc ciąg znaków o potencjalnym rozmiarze liczonym w gigabajtach. Pobieranie znaków kończy się, gdy program zczyta znak nowej linii. Powoduje to wypisanie całej zawartości tabeli na standardowe wyjście i zwolnienie pamięci, którą zajmowała.



Program zacząłem od napisania struktury reprezentującej bufor.

```c
typedef struct Buff {
	int cap; //       Obecna pojemność tabeli
	int len; //       Ilość znaków obecnie zapisanych w tabeli
	char *chars; //   Wskaźnik na tabelę
} Buff;
```



Następnie napisałem 3 funkcję, które korzystają z siebie i razem zajmują się dopisywaniem do tabeli kolejnych znaków oraz zwiększaniem jej rozmiaru, gdy jest to konieczne. Omówię je poniżej.



Pierwsza funkcja operuje na tabeli przy dość dużym poziomie abstakcji. Funkcja przyjmuje jako argument wskaźnik na strukturę "buff" i nic nie zwraca. Pobiera ze standardowego wejścia jeden znak i dopisuje go do tabeli w nieskończonej pętli, którą przerywa pobranie znaku nowej linii. Następnie, funkcja dopisuje do tabeli znak końca ciągu znaków ('\0').

```c
void fill(Buff *buff) {
	char current;
	while(1) {
		current = fgetc(stdin);
		append(buff, current);
		if (current == '\n') break;
	}
	append(buff, '\0');
}
```



Pierwsze znaki zarządzania pamięcią pojawiają się w funkcji append, która przed dopisaniem znaku do listy musi sprawdzić czy jest miejsce na dopis. Funkcja jako argumenty przyjmuje wskaźnik na strukturę i znak (char) "x". Jeśli obecna ilość znaków tabeli jest równa lub większa pojemności tabeli, funkcja rozszerza tabelę za pomocą funkcji resize. Następnie, funkcja append dopisuje do tabeli znak "x" i inkrementuje licznik wskazujący na ilość znaków w tabeli.

```c
void append(Buff *buff, char x) {
	if (buff->len >= buff->cap) {
		resize(buff);
	}
	*(buff->chars + buff->len++) = x;
}
```



Prawdziwe zarządzanie pamięcią odbywa się w ostatniej funkcji - resize. Jej zadaniem jest zwiększenie licznika pojemności tabeli o określoną ilość (10) a następnie wywołanie funkcji realloc z odpowiednimi argumentami. Jako argumenty podany jest wskaźnik na tabelę znaków oraz nowy rozmiar tabeli zapisany w zmiennej "cap" pomnożony przez rozmiar znaku (char) w bajtach. Rozmiar w zmiennej cap został przed chwilą zwiększony, więc to wywołanie funkcji zwiększy pojemność tabeli o 10. Wskaźnik jaki zwraca funkcja realloc jest przypisywany do zmiennej przechowującej wskaźnik na tabelę znaków. 

```c
void resize(Buff *buff) {
	buff->cap += STEPS;
	buff->chars = realloc(buff->chars, buff->cap * sizeof(char));
}
```



Funkcja main inicjuję strukturę, następnie wywołuje funkcję fill podając jej wskaźnik na strukturę. Po wykonaniu funkcji fill, main drukuje zawartość tabeli znaków i zwalnia pamięć, jaka jest jej przypisana. Program kończy swoje działanie.



---

## Mniejsze programy

### pdf-2.c

Prosty program, którego napisanie miało utrwalić wiedzę o funkcji malloc 

```c
#include <stdio.h>
#include <stdlib.h>

int main(void){
	int *x = malloc(sizeof(int));
	*x = 5;
	printf("%d\n", *x);
	free(x);
}
```

### pdf-4.c

Prosty program, którego napisanie miało utrwalić wiedzę o funkcji calloc.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void){
	char *str = calloc(6, sizeof(char));
	strcpy(str, "tekst");
	printf("%s\n", str);
	free(str);
}
```

### pdf-10.c

Prosty program, którego napisanie miało utrwalić wiedzę o funkcji calloc.

```c
#include <stdio.h>
#include <stdlib.h>

int main(void){
	int *pi;
	pi = calloc(5, sizeof(int));
	free(pi);
}
```


