#include <stdio.h>
#include <stdlib.h>

typedef struct Struktura {
	char imie[20];
	char nazwisko[20];
	int wiek;
} Struktura;

void scan_struct(Struktura *struktura) {
	scanf("%s", struktura->imie);
	scanf("%s", struktura->nazwisko);
	scanf("%d", &(struktura->wiek));
};

void print_struct(Struktura *struktura) {
	printf(
			"imie, nazwisko, wiek -> %s, %s, %d\n",
			struktura->imie,
			struktura->nazwisko,
			struktura->wiek
	);
};

int main(void){
	Struktura *struktura;
	struktura = malloc (sizeof(Struktura));

	puts("imie, nazwisko, wiek:");

	scan_struct(struktura);
	print_struct(struktura);
}
