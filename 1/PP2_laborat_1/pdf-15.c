#include <stdio.h>
#include <stdlib.h>

#define STEPS 10

typedef struct Buff {
	int cap;
	int len;
	char *chars;
} Buff;

void resize(Buff *buff) {
	buff->cap += STEPS;
	buff->chars = realloc(buff->chars, buff->cap * sizeof(char));
}

void append(Buff *buff, char x) {
	if (buff->len >= buff->cap) {
		resize(buff);
	}
	*(buff->chars + buff->len++) = x;
}

void fill(Buff *buff) {
	char current;
	while(1) {
		current = fgetc(stdin);
		append(buff, current);
		if (current == '\n') break;
	}
	append(buff, '\0');
}

int main(void){
	Buff buff;
	buff.cap = 10;
	buff.len = 0;
	buff.chars = calloc(buff.cap, sizeof(char));
	
	fill(&buff);

	printf("%s", buff.chars);

	free(buff.chars);
}
