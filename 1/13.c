#include <stdio.h>
#include <stdlib.h>

int pow2(int x) {
    return x * x;
}

int main(void) {
    int (*func)(int) = &pow2;
    
    printf("%d\n", func(9) );
}