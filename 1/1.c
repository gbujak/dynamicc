#include <stdio.h>
#include <stdlib.h>

int main (void) {
    int *val = malloc(sizeof(int));
    if (!val) {
        puts("blad przydzielania pamieci");
        exit(1);
    }

    *val = 5;
    printf("%d\n", *val);
    free(val);
    val = 0;

    return 0;
}