#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
    char *greeting = (char *) malloc(strlen("Witaj")+1);
    char *temp = greeting;
    strcpy(greeting,"Witaj");
    while (*greeting !=0) {
        printf("%c",*greeting);
        greeting++;
    }
    free(temp);
    return 0;
}