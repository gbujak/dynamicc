#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *trim(char *phrase) {
    char *oldString = phrase;
    char *newString = phrase;
    while (*oldString == ' ') {
        oldString++;
    }
    while (*oldString) {
        *(newString++) = *(oldString++);
    }
    *newString = 0;
    return (char *)realloc(phrase, strlen(phrase) + 1);
}

int main(void) {
    char *buffer =
        (char *)malloc(strlen(" ciąg znaków ze spacjami na początku") + 1);
    strcpy(buffer, " ciąg znaków ze spacjami na początku");
    printf("%s\n", buffer);
    printf("%s\n", trim(buffer));
    return 0;
}