#include <stdio.h>
#include <stdlib.h>

#define STEPS 10

typedef struct Buffer {
    int capacity;
    int length;
    char *chars;
} Buffer;

void increase(Buffer *buff) {
    buff -> capacity += STEPS;
    buff -> chars = realloc(buff->chars, buff->capacity * sizeof(char));
    if (buff->chars == NULL) {
        puts("realloc error");
        exit(1);
    }
}

void append(Buffer *buff, char x) {
    if (buff->length >= buff->capacity) {
        increase(buff);
    }
    *(buff->chars + (buff->length++)) = x;
}

void fill_buff(Buffer *buff) {
    char current;
    while (1) {
        current = fgetc(stdin);
        append(buff, current);
        //if (current == '\n') break;
    }
    append(buff, '\0');
}

int main(void) {
    Buffer buff;
    buff.capacity = 10;
    buff.length = 0;
    buff.chars = (char *)calloc(buff.capacity, sizeof(char));
    if (buff.chars == NULL) {
        puts("malloc error");
        exit(1);
    }

    fill_buff(&buff);
    printf("%s", buff.chars);

    free(buff.chars);
}