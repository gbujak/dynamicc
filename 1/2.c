#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int *pi;
    *pi = (int*)malloc(sizeof(int));
    free(pi);
    return 0;
}

/*
2.c:6:9: warning: assignment to ‘int’ from ‘int *’ makes
integer from pointer without a cast [-Wint-conversion]
     *pi = (int*)malloc(sizeof(int));

*pi to dereferencja wskaźnika - wartość zwracaną przez funkcję
malloc program przypisze zmiennej pi, a nie zmiennej przechowującej
wskaźnik na zmienną pi.

pod pi (int) zostanie podstawiony wskaźnik na int (int *)
adres wolnej pamięci zostanie podstawiony na adres wskazany
przez "dziki wskaźnik" *pi
*/

