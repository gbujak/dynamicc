#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define LEN 16

struct list {
    char *arr;
    unsigned len;
    unsigned cap;
};

struct list *makelist(unsigned cap) {
    struct list *l = malloc(sizeof *l);
    if (l == NULL) { puts("malloc error"); abort(); }
    l->cap = cap;
    l->len = 0;
    l->arr = calloc(cap, sizeof(char));
    if (l->arr == NULL) { puts("calloc error"); abort(); }
    l->arr[0] = '\0';
    return l;
}

bool add(struct list *l, char data) {
    if (l->len == l->cap) {
        l->cap *= 2;
        l->arr = realloc(l->arr, l->cap * sizeof(char));
        if (l->arr == NULL) {
            puts("realloc error");
            abort();
        }
    }
    l->arr[ l->len++ ] = data;
    l->arr[ l->len   ] = '\0';
    return true;
}

void delete(struct list *l, unsigned i) {
    if (i == l->len) return;
    while (i < l->len) {
        l->arr[i] = l->arr[i+1];
        i++;
    }
    l->len--;
}

void rem(struct list **lp) {
    struct list *l = *lp;
    free(l->arr);
    free(l);
    *lp = NULL;
}

int main (void) {
    struct list *l = makelist(LEN);

    {
        char inp;
        while ((inp = fgetc(stdin)) != '\n') add(l, inp);
    }
    
    printf("%s\n", l->arr);
    delete(l, 0);
    printf("%s\n", l->arr);
}