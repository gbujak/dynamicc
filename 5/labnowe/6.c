#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define LEN 512

struct list {
    char arr[LEN];
    unsigned len;
    unsigned cap;
};

bool add(struct list *l, char data) {
    if (l->len == l->cap) return false;
    l->arr[l->len++] = data;
    l->arr[l->len] = '\0';
}

void delete(struct list *l, unsigned i) {
    if (i == l->len) return;
    while (i < l->len) {
        l->arr[i] = l->arr[i+1];
        i++;
    }
    l->len--;
}

void rem(struct list *l) {
    l->len = 0;
    l->arr[0] = '\0';
}

int main (void) {
    struct list l;
    l.len = 0;
    l.cap = LEN;
    l.arr[0] = '\0';

    {
        char inp;
        while ((inp = fgetc(stdin)) != '\n') add(&l, inp);
    }
    
    printf("%s\n", l.arr);
    delete(&l, 0);
    printf("%s\n", l.arr);
    rem(&l);
    printf("%s\n", l.arr);
}






