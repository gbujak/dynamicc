# PP2 - Listy sprawozdanie

### Grzegorz Bujak



#### 2.c

Celem zadania było przekształcenie kodu źródłowego podanego w instrukcji tak, aby niektóre funkcje operujące na listach nie zwracały nowych wskaźników tylko zmieniały wskaźniki w funkcji wywoływującej za pomocą wskaźnika na wskaźnik. Zwracany miał być typ bool, wskazujący na pomyślne wykonanie funkcji. Zdążyłem przekształcić tylko jedną z funkcji.

```c
bool sll_node * insert_node ( struct sll_node ** frontp , int data ) {
    struct ssl_node *front = *frontp;
    if ( NULL == front )
        return false ;
    struct sll_node * new_node = ( struct sll_node *)
        malloc ( sizeof ( struct sll_node ) ) ;
    if ( NULL != new_node ) {
        new_node - > data = data ;
        new_node - > next = NULL ;
        if ( front - > data <= data )
            return insert_front ( front , new_node ) ;
        else {
            struct sll_node * node = find_spot ( front , data ) ;
            if ( NULL != node - > next )
                insert_after ( node , new_node ) ;
            else insert_back ( node , new_node ) ;
        }
    } else return false;
    *frontp = front;
    return true;
}
```



#### 5.c

Celem zadania było napisanie programu zapisującego znaki w liście oraz sprawdzającego, czy ciąg tych znaków jest palindromem. Funkcjonalność programu znajduje się głównie w funkcjach:

* getprev - dostając jednokierunkową listę oraz jej element, zwraca poprzedni element

* palindrome - dostając listę, sprawdza, czy ciąg znaków, jaki przechowuje jest palindromem (korzysta z funkcji getprev)

```c
struct node *getprev(struct list *l, struct node *n) {
    struct node *i;
    for (i = l->start; i->next != n; i = i->next);
    return i;
}

bool palindrome(struct list *l) {
    struct node *left, *right;
    left = l->start;
    right = l->end;

    while (1) {
        if (left->data != right->data) return false;
        else if (
            left->next == right || left == right
        ) return true;
        right = getprev(l, right);
        left = left->next;
    }
}
```

implementacja listy:

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
    char data;
    struct node *next;
};

struct list {
    struct node *start;
    struct node *end;
};

void append(struct list *l, char data) {
    struct node *temp = malloc(sizeof *temp);
    temp->data = data;
    temp->next = NULL;
    if (l->start == NULL) l->start = temp;
    else l->end->next = temp;
    l->end = temp;
}
```

funkcja main:

```c
int main (void) {
    struct list l = {
        NULL, NULL
    };
    char inp;
    while((inp = getc(stdin)) != '\n')
        append(&l, inp);
    printf("%d\n", palindrome(&l));
}
```

#### 4.c

Celem zadania było napisanie programu zapisującego dane w liście oraz funkcji, która odwraca kolejność tych danych jedynie przez przepinanie wskaźników. Funkcjonalność programu znajduje się głównie w funkcji rev. Implementacja listy identyczna do tej w zadaniu 5.

```c
struct node *rev(struct list *l) {
    struct node *prev, *curr, *next;
    prev = NULL;
    curr = l->start; next = curr->next;
    while (1) {
        curr->next = prev;
        prev = curr;
        curr = next;
        if (next->next == NULL) {
            next->next = prev;
            return next;
        } else next = next->next;
    }
}

void printn(struct node *n) {
    while (n != NULL) {
        printf("%c, ", n->data);
        n = n->next;
    }
    puts("");
}

int main (void) {
    struct list l = {
        NULL, NULL
    };
    char inp;
    while((inp = getc(stdin)) != '\n')
        append(&l, inp);
    printf("%d\n", palindrome(&l));
}
```



#### 6.c

Program implementuje listę za pomocą statycznie alokowanej tabeli. Implementacja zawiera funkcje:

* add - dodanie elementu do końca listy

* delete - usunięcie elementu o podanym indeksie w tablicy

* rem - wyczyszczenie listy

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define LEN 512

struct list {
    char arr[LEN];
    unsigned len;
    unsigned cap;
};

bool add(struct list *l, char data) {
    if (l->len == l->cap) return false;
    l->arr[l->len++] = data;
    l->arr[l->len] = '\0';
}

void delete(struct list *l, unsigned i) {
    if (i == l->len) return;
    while (i < l->len) {
        l->arr[i] = l->arr[i+1];
        i++;
    }
    l->len--;
}

void rem(struct list *l) {
    l->len = 0;
    l->arr[0] = '\0';
}

int main (void) {
    struct list l;
    l.len = 0;
    l.cap = LEN;
    l.arr[0] = '\0';
    {
        char inp;
        while ((inp = fgetc(stdin)) != '\n') add(&l, inp);
    }    
    printf("%s\n", l.arr);
    delete(&l, 0);
    printf("%s\n", l.arr);
    rem(&l);
    printf("%s\n", l.arr);
}
```




























































