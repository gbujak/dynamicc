#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
    char data;
    struct node *next;
};

struct list {
    struct node *start;
    struct node *end;
};

void append(struct list *l, char data) {
    struct node *temp = malloc(sizeof *temp);
    temp->data = data;
    temp->next = NULL;
    if (l->start == NULL) l->start = temp;
    else l->end->next = temp;
    l->end = temp;
}

struct node *getprev(struct list *l, struct node *n) {
    struct node *i;
    for (i = l->start; i->next != n; i = i->next);
    return i;
}

bool palindrome(struct list *l) {
    struct node *left, *right;
    left = l->start;
    right = l->end;

    while(1) {
        if (left->data != right->data) return false;
        else if (
            left->next == right || left == right
        ) return true;
        right = getprev(l, right);
        left = left->next;
    }
}

int main (void) {
    struct list l = {
        NULL, NULL
    };
    char inp;
    while((inp = getc(stdin)) != '\n')
        append(&l, inp);
    printf("%d\n", palindrome(&l));
}
