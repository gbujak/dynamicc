#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
    char data;
    struct node *next;
};

struct list {
    struct node *start;
    struct node *end;
};

void append(struct list *l, char data) {
    struct node *temp = malloc(sizeof *temp);
    temp->data = data;
    temp->next = NULL;
    if (l->start == NULL) l->start = temp;
    else l->end->next = temp;
    l->end = temp;
}

void printn(struct node *n) {
    while (n != NULL) {
        printf("%c, ", n->data);
        n = n->next;
    }
    puts("");
}

struct node *getprev(struct list *l, struct node *n) {
    if (n == l->start) return NULL;
    struct node *i;
    for (i = l->start; i->next != n; i = i->next);
    return i;
}

struct node *rev(struct list *l) {
    struct node *prev, *curr, *next;
    prev = NULL;
    curr = l->start; next = curr->next;
    while (1) {
        curr->next = prev;
        prev = curr;
        curr = next;
        if (next->next == NULL) {
            next->next = prev;
            return next;
        } else next = next->next;
    }
}

int main (void) {
    struct list l = {
        NULL, NULL
    };
    char inp;
    while((inp = getc(stdin)) != '\n')
        append(&l, inp);
    printn(l.start);
    struct node *lrev = rev(&l);
    printn(lrev);
}







