#include <stdio.h>

long tribonacci (int n) {
    if (n<2) return 0;
    else if (n==2) return 1;
    else return
          tribonacci(n-1)
        + tribonacci(n-2)
        + tribonacci(n-3);
}

int main(void) {
    int n;
    scanf("%d", &n);
    printf("%d\n", tribonacci(n));
}