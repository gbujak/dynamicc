#include <stdio.h>

double carpet(double field, int recursion) {
    if (recursion == 0) return field;
    else return 8.0/9.0 * carpet(field, recursion - 1);
}

int main(void) {
    puts("pole?");
    double field;
    scanf("%lf", &field);

    for (int i = 0; i <= 20; i++)
        printf("%d -> %lf", i, carpet(field, i));
}
