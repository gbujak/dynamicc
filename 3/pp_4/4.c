#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define LEN 50

int find(int to_find, int arr[], int index) {
    if (index == LEN) return 0;
    else if (arr[index] == to_find)
         return  1 + find(to_find, arr, index + 1);
    else return  0 + find(to_find, arr, index + 1);
}

int main(void) {
    srand( time(NULL) );
    int arr[LEN];

    for (int i = 0; i < LEN; i++)
        arr[i] = rand() % 21;

    int to_find;
    scanf("%d", &to_find);

    printf("%d\n", find(to_find, arr, 0));

    for (int i = 0; i < LEN; i++)
        printf("%d, ", arr[i]);
    puts(""); 
}
