#include <stdio.h>

void line(int x) {
    putc('-', stdout);
    if (x == 0) {
       putc('+', stdout);
       putc('*', stdout);
       return;
    }
    line(x-1);
    putc('*', stdout);
}

int main (void) {
    for (int i = 0; i <= 10; i++) {
        line(i);
        puts("");
    }
}
