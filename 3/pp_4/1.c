#include <stdio.h>

int tribonacci(int n) {
    if (n < 2) return 0;
    else if (n == 2) return 1;
    else return tribonacci(n-1) + tribonacci(n-2) + tribonacci(n-3);
}

int main (void) {
    for (int i = 0; i <= 10; i++)
        printf("%d\n", tribonacci(i));
}
