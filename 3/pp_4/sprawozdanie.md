# Sprawozdanie PP2 laboratorium 4

### 1.c

Program liczy n-ty element ciągu tribonacciego dla n z \<0, 10\>. Robi to rekurencyjnie - gdy n \> 0, zwraca sumę wyników samej siebie z argumentami n-1, n-2, n-3.

```c

int tribonacci(int n) {
    if (n < 2) return 0;
    else if (n == 2) return 1;
    else return tribonacci(n-1) 
              + tribonacci(n-2)
              + tribonacci(n-3);
}

int main (void) {
    for (int i = 0; i <= 10; i++)
        printf("%d\n", tribonacci(i));
}
```

### 2.c

Program liczy całkowitą długość spirali, której najdłuższy bok ma długość a, a każdy kolejny jest o 2 krótszy. Wartość jest liczona dla a od 0 do 50.

```c
#include <stdio.h>

int spiral(int a) {
    if (a <= 0) return 0;
    else return a + spiral(a-2);
}

int main(void) {
    puts("pierwszy bok -> dlugosc calkowita");
    for (int i = 0; i < 50; i++)
        printf("%d -> %d\n", i, spiral(i));
}
```

### 3.c

Program liczy pole dywanu sierpińskiego o polu początkowym podanym przez użytkownika i rekurencji od 0 do 20. Pole dąży do zera, co widać na wartościach wydrukowanych przez program.

```c
#include <stdio.h>

double carpet(double field, int recursion) {
    if (recursion == 0) return field;
    else return 8.0/9.0 * carpet(field, recursion - 1);
}

int main(void) {
    puts("pole?");
    double field;
    scanf("%lf", &field);

    for (int i = 0; i <= 20; i++)
        printf("%d -> %lf", i, carpet(field, i));
}
```

### 4.c

Wypełnia losowo tablicę liczb całkowitych. Pobiera od użytkownika szukaną wartość. Rekurencyjnie szuka wystąpień tej liczbyw tabeli i zwraca ilość jej wystąpień. 

```c
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define LEN 50

int find(int to_find, int arr[], int index) {
    if (index == LEN) return 0;
    else if (arr[index] == to_find)
         return  1 + find(to_find, arr, index + 1);
    else return  0 + find(to_find, arr, index + 1);
}

int main(void) {
    srand( time(NULL) );
    int arr[LEN];

    for (int i = 0; i < LEN; i++)
        arr[i] = rand() % 21;

    int to_find;
    scanf("%d", &to_find);

    printf("%d\n", find(to_find, arr, 0));

    for (int i = 0; i < LEN; i++)
        printf("%d, ", arr[i]);
    puts(""); 
}
```

### 5_2.c

Program rekurencyjnie drukuje na ekran ciąg znaków, który składa się z 

* n znaków '-'

* jednego znaku '+'

* n znaków '*'

znaki - są drukowane, gdy program wchodzi w kolejne funkcje, a *, gdy wychodzi z nich.

```c
#include <stdio.h>

void line(int x) {
    putc('-', stdout);
    if (x == 0) {
       putc('+', stdout);
       putc('*', stdout);
       return;
    }
    line(x-1);
    putc('*', stdout);
}

int main (void) {
    for (int i = 0; i <= 10; i++) {
        line(i);
        puts("");
    }
}
```
