#include <stdio.h>

void line(int x, int len) {
    if (x > len) return;
    if (x < 0) {
        putc('-', stdout);
        line(x + 1, len);
    } else if (x == 0) {
        putc('+', stdout);
        line(x + 1, len);
    } else if (x > 0) {
        putc('*', stdout);
        line(x+1, len);
    }
}

int main (void) {
    for (int i = 0; i <= 10; i++) {
        line(-i, i);
        puts("");
    }
}
