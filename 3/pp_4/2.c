#include <stdio.h>

int spiral(int a) {
    if (a <= 0) return 0;
    else return a + spiral(a-2);
}

int main(void) {
    puts("pierwszy bok -> dlugosc calkowita");
    for (int i = 0; i < 50; i++)
        printf("%d -> %d\n", i, spiral(i));
}