#include <stdio.h>

int coil(int a) {
    if (a<=0)
        return 0;
    else return a + coil(a - 2);
}

int main(void) {
    int a;
    scanf("%d", &a);
    printf("%d\n", coil(a));    
}