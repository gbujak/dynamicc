#include <stdio.h>

double carpet(double field, int recursion) {
    if (recursion == 0) return field * 8.0/9.0;
    else return carpet(field * 8.0/9.0, recursion - 1);
}

/*
carpet 1 1 = 8/9
carpet 1 0 = 1
carpet 1 2 = (8/9)^2
*/

int main (void) {
    int recursion;
    double field;
    scanf("%d", &recursion);
    scanf("%lf", &field);

    printf("%d, %lf\n", recursion, field);

    printf("%lf\n", carpet(field, recursion));
}